local treesitter = require('nvim-treesitter.configs')

treesitter.setup {
  ensure_installed = {
    'lua',
    'javascript',
    'jsdoc',
    'typescript',
    'tsx',
    'comment',
    'html',
    'css',
    'scss',
    'yaml',
    'toml',
    'dockerfile',
    'markdown',
  },
  highlight = {
    enable = true,
    additional_vim_regex_highlighting  = false,
  },
  indent = {
    enable = true,
  },
}
