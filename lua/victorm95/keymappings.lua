local telescope = require('telescope.builtin')
local options = { noremap = true, silent = true }

-- Leader key
vim.g.mapleader = ' '

-- Buffers
vim.keymap.set('n', '<Tab>', ':bnext<CR>', options)
vim.keymap.set('n', '<S-Tab>', ':bprev<CR>', options)

-- Editor
vim.keymap.set('i', '<C-j>', '<End><CR>', options)
vim.keymap.set('i', '<C-k>', '<Home><CR><Up>', options)
vim.keymap.set('n', '<Leader><Space>', ':nohl<CR>', options)
vim.keymap.set('n', '<Leader>p', '"_dP', options)
vim.keymap.set('n', '<C-e>', ':Ex<CR>', options)
vim.keymap.set('n', '<C-u>', '<C-u>zz', options)
vim.keymap.set('n', '<C-d>', '<C-d>zz', options)
vim.keymap.set('n', 'n', 'nzzzv', options)
vim.keymap.set('n', 'N', 'Nzzzv', options)

-- Quickfix
vim.keymap.set('n', '<Leader>qo', ':cop<CR>', options)
vim.keymap.set('n', '<Leader>qc', ':ccl<CR>', options)
vim.keymap.set('n', '<Leader>qn', ':cn<CR>', options)
vim.keymap.set('n', '<Leader>qp', ':cp<CR>', options)

-- Location list
vim.keymap.set('n', '<Leader>lo', ':lop<CR>', options)
vim.keymap.set('n', '<Leader>lc', ':lcl<CR>', options)
vim.keymap.set('n', '<Leader>ln', ':lne<CR>', options)
vim.keymap.set('n', '<Leader>lp', ':lp<CR>', options)

-- Telescope
vim.keymap.set('n', '<C-p>', telescope.find_files, options)
vim.keymap.set('n', '<Leader><C-p>', telescope.live_grep, options)
vim.keymap.set('n', '<C-b>', telescope.buffers, options)

-- Git
vim.keymap.set('n', '<Leader>gg', ':Git<CR>', options)
vim.keymap.set('n', '<Leader>gd', ':Gvdiffsplit<CR>', options)
vim.keymap.set('n', '<Leader>gb', ':Git blame<CR>', options)

-- Packer
vim.keymap.set('n', '<Leader>ps', ':PackerSync<CR>', options)
