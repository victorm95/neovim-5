local lspconfig = require('lspconfig')
local cmp = require('cmp')
local fidget = require('fidget')
local capabilities = require('cmp_nvim_lsp').default_capabilities()

fidget.setup()

cmp.setup {
  completion = { autocomplete = false },
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  window = {
    -- completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  }, {
    -- { name = 'buffer' },
  }),
}

local function on_attach(client, bufnr)
  local options = {
    buffer = bufnr,
    noremap = true,
    silent = true,
  }

  -- vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  vim.keymap.set('i', '<C-f>', cmp.complete, options)
  vim.keymap.set('i', '<C-Space>', cmp.complete, options)
  vim.keymap.set('n', '<C-h>', vim.lsp.buf.hover, options)
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, options)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, options)
  vim.keymap.set('n', 'gt', vim.lsp.buf.type_definition, options)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, options)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, options)
  vim.keymap.set('n', '<Leader>rn', vim.lsp.buf.rename, options)
  vim.keymap.set('n', '<Leader>ff', vim.lsp.buf.formatting, options)
  vim.keymap.set('n', '<Leader>qq', vim.diagnostic.setloclist, options)
  vim.keymap.set('n', ']d', vim.diagnostic.goto_next, options)
  vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, options)
end

lspconfig.tsserver.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}

local prettier = {
 formatCommand = './node_modules/.bin/prettier --stdin-filepath ${INPUT}',
 formatStdin = true,
}

local eslint = {
  lintCommand = './node_modules/.bin/eslint -f visualstudio --stdin --stdin-filename ${INPUT}',
  lintIgnoreExitCode = true,
  lintStdin = true,
  lintFormats = {'%f(%l,%c): %tarning %m', '%f(%l,%c): %rror %m'},
}

lspconfig.efm.setup {
  init_options = { documentFormatting = true },
  filetypes = {'javascript', 'typescript', 'javascriptreact', 'typescriptreact', 'javascript.jsx', 'typescript.tsx'},
  settings = {
    rootMarkers = {'.git/'},
    languages = {
      javascript = { eslint, prettier },
      javascriptreact = { eslint, prettier },
      typescript = { prettier },
      typescriptreact = { prettier },
    },
  },
}
