-- Editor
vim.opt.tabstop = 4
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.list = true
vim.opt.listchars = {
  eol = '⤸',
  tab = '››',
  lead = '⋅',
  trail = '⋅',
  extends = '…',
  precedes = '…',
}

-- Theme
vim.opt.termguicolors = true
vim.cmd [[colorscheme rose-pine]]

-- Files
vim.opt.fileencodings = 'utf-8'
vim.opt.fileformats = {'unix', 'dos'}

-- Misc
vim.opt.wrap = false
vim.opt.clipboard = 'unnamedplus'
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
vim.opt.hidden = true
vim.opt.guicursor = ''
vim.opt.cursorline = true
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 8
vim.opt.colorcolumn = {80}
vim.opt.undofile = true
vim.opt.swapfile = false


-- Packages setup
require('lualine').setup {
  options = {
    globalstatus = true,
    component_separators = '',
    section_separators = ' ',
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {{'branch', icon = ''}},
    lualine_c = {{'filename', path = 1}},
    lualine_x = {'diagnostics', '"[ %l:%c ]"', '%y'},
    lualine_y = {},
    lualine_z = {},
  },
}

require('telescope').setup {
  defaults = {
    layout_config = { width = 0.9 },
  },
  extensions = {
    fzy_native = {
      override_generic_sorter = false,
      override_file_sorter = true,
    },
  },
}
